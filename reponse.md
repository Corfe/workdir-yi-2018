# TP Realworld

## 6.3
### 6.3.1
SHA1 7a2e472d9d9e77ad7de8f4ace9e09d7597ee60f7

### 6.3.2
dockerfile url:  docker-npm/node-8-debian/Dockerfile 
volume: /src

cmd: docker pull mkenney/npm:node-8-debian
ID: 9af19a7f4e2c

### 6.3.3
cmd: docker container run -ti -v $(pwd):/src mkenney/npm:node-8-debian npm install

### 6.3.4
cmd: docker container run -ti -p 8000:8080 -v $(pwd):/src mkenney/npm:node-8-debian npm run dev

## 6.4
### 6.4.1
SHA1 5a11b9d391970a9be59d72f7e069b65c81f2118e

### 6.4.2
cmd: docker pull gradle:4.7.0-jdk8-alpine
id: f438b7d58d0a
volume: f438b7d58d0a

### 6.4.3
cmd creation volume: docker volume create gradle-home
cmd lister tous les volumes: docker volume ls
retour:
DRIVER              VOLUME NAME
local               0b3e1797ff7bcabc9838a2e1a711154b7f9a98e887b2e43d8bea080a265f3243
local               404ac1b8e5f867ee6bb9709c3112919eadf79d9e39e8c6842d7b9e08ee06ac31
local               b08641e5c767daea7943f89d981187efbdf6be576feefdc2ddb07bb305ad42d4
local               d64aece598a10a28461c248c16ea621d753a696df4cb02217c5359fa8af6860d
local               dfffd261af05fb8a3251442ef17e85d4111ff87ed7b462744507b5095455b104
local               e16df3bc89ece1156373849d83eb7e4de14bb9597c5cebba5e8de45689a2c3b5
local               gradle-home

### 6.4.4
cmd: docker container run -ti -w /src -v gradle-home:/home/gradle/.gradle -v $(pwd):/src gradle:4.7.0-jdk8-alpine gradle build

### 6.4.5
cmd: docker container run -ti -w /src -p 8001:8080 -v gradle-home:/home/gradle/.gradle -v $(pwd):/src gradle:4.7.0-jdk8-alpine gradle bootRun
retour:	
articles	[]
articlesCount	0

## 6.5
### 6.5.1
retour:
articles	
0	
id	"f36557cc-47ad-4fba-b970-2ce5a5e2dfb7"
slug	"l'exposition-des-bananes"
title	"L'exposition des bananes"
description	"Banane Lille"
body	"Super exposition"
favorited	false
favoritesCount	0
createdAt	"2018-05-16T13:02:23.897Z"
updatedAt	"2018-05-16T13:02:23.897Z"
tagList	[]
author	
username	"Edmond"
bio	""
image	"https://static.productionready.io/images/smiley-cyrus.jpg"
following	false
articlesCount	1

## 6.6
### 6.6.1
cmd: docker container run -ti -v $(pwd):/src mkenney/npm:node-8-debian npm run build
ls: favicons-c2a605fbc0e687b2e1b4b90a7c445cdd  index.html  static

### 6.6.2
cmd: docker container run -ti -v $(pwd)/dist:/usr/share/nginx/html -p 8000:80 nginx:alpine

### 6.6.3







